import React from "react"
import { Link, graphql } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"
// import Share from "../components/share"
import Tag from "../components/tag"
import { rhythm } from "../utils/typography"

class BlogPostTemplate extends React.Component {
    render() {
        const site = this.props.data.site.siteMetadata
        const post = this.props.data.markdownRemark
        const { previous, next } = this.props.pageContext

        return (
            <Layout location={this.props.location} title={site.title}>
            <SEO title={post.frontmatter.title} description={post.excerpt} />
            <h1>{post.frontmatter.title}</h1>
            <p>
            Par {post.frontmatter.author} le {post.frontmatter.date} - <small>{Math.ceil(post.fields.readingTime.minutes)} {Math.ceil(post.fields.readingTime.minutes) > 1 ? "minutes" : "minute"} de lecture</small>
            </p>

            <p>
            {post.frontmatter.tags.map((tag, index) => {
                return <Tag key={index} text={tag} />
            })}
            </p>

            {post.frontmatter.featuredImage ?
                <Img sizes={post.frontmatter.featuredImage.childImageSharp.sizes} /> :
                null
            }

            <div dangerouslySetInnerHTML={{ __html: post.html }} />
            <hr
            style={{
                marginBottom: rhythm(1),
            }}
            />

            <ul
            style={{
                display: `flex`,
                flexWrap: `wrap`,
                justifyContent: `space-between`,
                listStyle: `none`,
                padding: 0,
            }}
            >
            <li>
            {previous && (
                <Link to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
                </Link>
            )}
            </li>
            <li>
            {next && (
                <Link to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
                </Link>
            )}
            </li>
            </ul>

            <hr/>

            <p>
                Envie de commenter ou de contribuer ? Postez vos réactions sur le <a href="https://forum.parlement-ouvert.fr">Forum du Bureau Ouvert</a>.
            </p>
            </Layout>
        )
    }
}

export default BlogPostTemplate

export const pageQuery = graphql`
query BlogPostBySlug($slug: String!) {
    site {
        siteMetadata {
            title
            author
            social {
                url
                twitter
            }
        }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
        id
        excerpt(pruneLength: 200)
        html
        fields {
            slug
            readingTime {
                minutes
            }
        }
        frontmatter {
            title
            author
            tags
            date(formatString: "DD MMMM YYYY", locale: "fr")
            featuredImage {
                childImageSharp{
                    sizes(maxWidth: 630) {
                        ...GatsbyImageSharpSizes
                    }
                }
            }
        }
    }
}
`
