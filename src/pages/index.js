import React from "react"
import { Link, graphql } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Tag from "../components/tag"
import { rhythm } from "../utils/typography"

class BlogIndex extends React.Component {
    render() {
        const { data } = this.props
        const siteTitle = data.site.siteMetadata.title
        const posts = data.allMarkdownRemark.edges

        return (
            <Layout location={this.props.location} title={siteTitle}>
            <SEO
                title="Accueil"
                keywords={[`bureau ouvert`, `blog`, `assemblée nationale`]}
            />

            {posts.map(({ node }) => {
                const title = node.frontmatter.title || node.fields.slug

                return (
                    <div key={node.fields.slug}>
                    <h3
                        style={{
                            marginBottom: rhythm(1 / 4),
                        }}
                    >
                        <Link style={{ boxShadow: `none` }} to={node.fields.slug}>{title}</Link>
                    </h3>

                    <small>
                        Par {node.frontmatter.author} le {node.frontmatter.date} - {Math.ceil(node.fields.readingTime.minutes)} {Math.ceil(node.fields.readingTime.minutes) > 1 ? "minutes" : "minute"} de lecture
                    </small>

                    <p>
                        {node.frontmatter.tags.map((tag, index) => {
                            return <Tag key={index} text={tag} />
                        })}
                    </p>

                    <hr/>
                    {node.frontmatter.featuredImage ?
                        <Img sizes={node.frontmatter.featuredImage.childImageSharp.sizes} /> :
                        null
                    }
                    <p dangerouslySetInnerHTML={{ __html: node.excerpt }} />
                    </div>
                )
            })}
            </Layout>
        )
    }
}

export default BlogIndex

export const pageQuery = graphql`
query {
    site {
        siteMetadata {
            title
        }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
        edges {
            node {
                excerpt(pruneLength: 400)
                fields {
                    slug
                    readingTime {
                        minutes
                    }
                }
                frontmatter {
                    date(formatString: "DD MMMM YYYY", locale: "fr")
                    title
                    author
                    tags
                    featuredImage {
                        childImageSharp{
                            sizes(maxWidth: 630) {
                                ...GatsbyImageSharpSizes
                            }
                        }
                    }
                }
            }
        }
    }
}
`
