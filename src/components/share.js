import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	TwitterShareButton,
	// FacebookShareButton,
	// GooglePlusShareButton,
	// LinkedinShareButton,
	// WhatsappShareButton,
	// RedditShareButton,
} from 'react-share';

// import './share.scss';

// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
// import { faTwitter } from '@fortawesome/free-brands-svg-icons'

// library.add(faStroopwafel, faTwitter)

const Share = ({ socialConfig, tags }) => (
	<div className="post-social">
		<TwitterShareButton url={socialConfig.config.url} className="button is-outlined is-rounded twitter" title={socialConfig.config.title} via={socialConfig.twitterHandle.split('@').join('')} hashtags={tags} >
			<span className="icon">
				<FontAwesomeIcon icon={['fab', 'twitter']} />
			</span>
			<span className="text">Twitter</span>
		</TwitterShareButton>
	</div>
);

Share.propTypes = {
	socialConfig: PropTypes.shape({
		twitterHandle: PropTypes.string.isRequired,
		config: PropTypes.shape({
			url: PropTypes.string.isRequired,
			title: PropTypes.string.isRequired,
		}),
	}).isRequired,
	tags: PropTypes.arrayOf(PropTypes.string),
};

Share.defaultProps = {
	tags: [],
};

export default Share;
