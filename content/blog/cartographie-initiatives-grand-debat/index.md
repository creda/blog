---
title: Initiatives parallèles au Grand Débat National
date: "2019-03-01T12:00:00.000"
author: "Mauricio Mejia"
featuredImage: "./thumbnail.png"
tags: ["Grand Débat", "Données"]
---

La crise des gilets jaunes a montré la nécessité des citoyens de pouvoir s'exprimer dans les décisions politiques de la France. Pour répondre à ce besoin, le Gouvernement a lancé en janvier dernier le Grand Débat National. En physique, en ligne, tous les citoyens sont invités à s'exprimer sur 4 thématiques : fiscalité et dépenses publiques, transition écologique, organisation de l'Etat et des services publics, démocratie et citoyenneté.<!-- end -->

Pour être complétement transparent, les données du Grand Débat National sont ouvertes en open data. tout un chacun peut donc s'en emparer pour les faire parler différemment.

Ci-dessous, retrouvez des initiatives de citoyens et de pouvoirs publics qui ont réutilisé ces données. Data visualition, plateforme d'annotation, agrégation de citations, laissez vous surprendre par le potentiel des données.

Si vous êtes du côté réutilisateur, n'hésitez pas à enrichir cette liste pour développer les débats.

##Niveau National

* Le Vrai Débat : https://le-vrai-debat.fr/
* Nous Citoyens : http://www.nouscitoyens.fr/forum/grand-debat-national/
* Politizr : https://www.politizr.com/groupes/le-grand-debat-national
* Association pour le Dialogue Social Territorial à La Réunion (ADSTR) : https://contribuer-adstr.fr/
* Le Frenc Debat (jeunes 15 - 25) : http://lefrenchdebat.fr/le-grand-debat/
* APF France handicap : https://participer.apf-francehandicap.org/themes/le-grand-debat-national
* MODEM : https://www.agoradem.fr/agoradem/les-themes/
* Make.org : https://about.make.org/grande-consultation-nationale-quelles-sont-vos-propositions
* Place Publique : https://consultation.place-publique.eu/projects
* La République en Marche : https://en-marche.fr/atelier-des-idees/contribuer
* Entendre la France : https://reponses.entendrelafrance.fr/
* Cahiers de doléances, maires ruraux de France : https://www.amrf.fr/wp-content/uploads/sites/46/2019/01/Synth%C3%A8se-Globale-V2.pdf
* Cahiers de doléances, Fluicity : https://www.flui.city/amrf/projects/29009-vos-doleances-vos-propositions
* Gilets Jaunes, Fluicity : https://www.flui.city/gilets-jaunes/news
* Mes Opinions : https://www.mesopinions.com/grand-debat
* ParlonsRIC (référendum d'initiative citoyenne) : https://parlement-et-citoyens.fr/consultation/referendum-dinitiative-citoyenne/presentation/presentation-18

##Niveau Local

* Lille Métropôle : https://participation.lillemetropole.fr/processes/granddebatmel
* Nancy : https://participez.nancy.fr/processes/GrandDebatNational (ouverture des données à partir du 16 mars)
* Chambre de Commerce et d'Industrie Marseille Provence : https://tousacteurs.ccimp.com
* Mairie de Paris : https://idee.paris.fr/project/conference-de-consensus-parisien-ne-s-exprimez-votre-opinion/collect/deposer-ici-vos-contributions-au-debat-national


##Liste d'exemples de réutilisations :

* https://twitter.com/sebsemdee/status/1093890918766252032?s=21

* https://medium.com/@quentin.maire/lobbies-d%C3%A9put%C3%A9s-citoyens-qui-sont-les-acteurs-du-grand-d%C3%A9bat-23109a9acea2

* https://www.data.gouv.fr/fr/reuses/contributions-rapportees-a-la-population-par-departement-sur-granddebat-fr/

* https://www.data.gouv.fr/fr/reuses/analyse-simple-des-questions-fermees/
