---
title: Lancement du Blog
date: "2019-03-01T12:00:00.000"
author: "Alexis Thual"
featuredImage: "./thumbnail.png"
tags: ["Blog", "Bureau Ouvert"]
---

Le Bureau Ouvert lance un blog destiné à publier des articles qui éclairent le fonctionnement du parlement et réagissent à l'actualité politique.

Nous affichons une ligne non-partisane et invitons toutes celles et tous ceux qui souhaient participer à nous rejoindre dans la rédaction de ces articles !<!-- end -->

L'intégralité du code du blog et des articles est [disponible librement sur Framagit](https://framagit.org/parlement-ouvert/blog). Les articles sont rédigés en [Markdown](https://fr.wikipedia.org/wiki/Markdown) et le blog basé sur [GatsbyJS](https://www.gatsbyjs.org) afin de pouvoir intégrer des visualisations complètes à nos articles.
La plateforme [HackMD](https://hackmd.io) pourra vous être utile pour rédiger de nouveaux articles, de même que [cette fiche explicative de Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
